#ifndef CONNECTPAGE_H
#define CONNECTPAGE_H

#include "../src/SqlUtils.h"
#include "WizardArea.h"
#include "mainwindow.h"

#include <QTableView>
#include <memory>

class ConnectPage: public QWizardPage
{
    Q_OBJECT

public:
    ConnectPage(QWidget* parent = 0);
    ~ConnectPage();

    int nextId() const;

private:
    std::shared_ptr<SqlAdapter> history_sql;
    std::shared_ptr<SqlAdapter> time_line_sql;
    
    QLabel* status_msg;
    QTableView* view;
    QModelIndex current_index;
    QString current_db_name;
    QString current_db_path;
    QPushButton* delete_button;
    QPushButton* connect_button;
    QItemSelectionModel* selection;

    MainWindow* w;

public slots:
    void show_time_line();
    void delete_table_entry();
    void change_current(QModelIndex current, QModelIndex previous);
};

#endif // CONNECTPAGE_H

#include "connectpage.h"
#include "sqltablemodel.h"

#include <QHeaderView>

using namespace std;

ConnectPage::ConnectPage(QWidget *parent) :
    QWizardPage(parent)
{
    setTitle(tr("Connect Page"));

    status_msg = new QLabel(tr(" "));

    history_sql = make_shared<SqlAdapter>("history_bd");
    history_sql->connect();

    QStringList col_names;
    col_names << "bd_name" << "img_path";
    HistoryModel* m = new HistoryModel(history_sql, col_names);
    view = new QTableView;
    view->setModel(m);
    view->horizontalHeader()->setStretchLastSection(true);

    delete_button = new QPushButton(tr("Delete"));
    connect_button = new QPushButton(tr("Connect"));

    QHBoxLayout* button_layout = new QHBoxLayout;
    QVBoxLayout* layout = new QVBoxLayout;
    layout->addWidget(view);
    layout->addWidget(status_msg);
    layout->addLayout(button_layout);
    button_layout->addWidget(delete_button);
    button_layout->addWidget(connect_button);
    setLayout(layout);

    selection = new QItemSelectionModel(view->model());
    view->setSelectionModel(selection);

    connect(connect_button, SIGNAL(clicked()), this, SLOT(show_time_line()));
    connect(delete_button, SIGNAL(clicked()), this, SLOT(delete_table_entry()));
    connect(selection, SIGNAL(currentRowChanged(QModelIndex,QModelIndex)), this, SLOT(change_current(QModelIndex, QModelIndex)));
}

int ConnectPage::nextId() const
{
    return -1;
}

void ConnectPage::change_current(QModelIndex current, QModelIndex previous)
{
    current_index = current;
    current_db_name = view->model()->data(view->model()->index(current.row(), 0)).toString();
    current_db_path = view->model()->data(view->model()->index(current.row(), 1)).toString();
}

void ConnectPage::show_time_line()
{
    status_msg->setText(tr(" "));
    if (current_db_name.size() != 0 && current_db_path.size() != 0) {
        time_line_sql = make_shared<SqlAdapter>(current_db_name.toStdString());
        if (time_line_sql->connect() == 1) {
            status_msg->setText("Ooh! Database with this image doesn`t exist right now! Try another.");
            return;
        }
        QStringList col_names;
        col_names << "Time" << "Name" << "Event" << "Size" << "Path" << "Type";
        TimeLineTableModel* m = new TimeLineTableModel(time_line_sql, col_names, wizard_area::max_elements_in_view);
        QTableView* tl_view = new QTableView;
        tl_view->setModel(m);

        w = new MainWindow(tl_view);
        w->show();
    }
}

void ConnectPage::delete_table_entry()
{
    view->model()->removeRows(current_index.row(), 1);
}

ConnectPage::~ConnectPage()
{
    delete view;
    delete delete_button;
    delete connect_button;
    delete status_msg;
    delete selection;
    delete w;
}

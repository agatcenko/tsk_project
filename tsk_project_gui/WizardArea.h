#ifndef WIZARDAREA_H
#define WIZARDAREA_H

#include <QWizard>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QRadioButton>
#include <QFileDialog>
#include <QVBoxLayout>

namespace wizard_area {
    const int max_elements_in_view = 10000;

    enum {Page_Start, Page_Init, Page_Connect};
}

#endif // WIZARDAREA_H

#ifndef STARTPAGE_H
#define STARTPAGE_H

#include "WizardArea.h"

class StartPage : public QWizardPage
{
    Q_OBJECT

public:
    StartPage(QWidget* parent = 0);
    ~StartPage();

    int nextId() const;

private:
    QVBoxLayout* layout;
    QRadioButton* init_button;
    QRadioButton* connect_button;
};

#endif // STARTPAGE_H

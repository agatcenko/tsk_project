#include "startwindow.h"

StartWindow::StartWindow(QWidget *parent) :
    QWizard(parent)
{
    setPage(wizard_area::Page_Start, new StartPage);
    setPage(wizard_area::Page_Init, new InitPage);
    setPage(wizard_area::Page_Connect, new ConnectPage);
    setStartId(wizard_area::Page_Start);
}

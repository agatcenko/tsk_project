#include "mainwindow.h"
#include "startwindow.h"

#include "../src/ImgReader.h"
#include "../src/SqlUtils.h"
#include "../src/MakeQuery.h"

#include <QApplication>
#include <QTextCodec>
#include <QtGui>

#include <iostream>
#include <sstream>
#include <memory>

using namespace std;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QTextCodec* codec = QTextCodec::codecForName("UTF-8");
    codec->setCodecForCStrings(codec);

    QFile style_sheet("StyleSheet.qss");
    style_sheet.open(QFile::ReadOnly);
    a.setStyleSheet(QString(style_sheet.readAll()));

    StartWindow start;
    start.show();

    return a.exec();
}

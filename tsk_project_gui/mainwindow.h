#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTableView>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

    QTableView* table;
    QItemSelectionModel* selection;
    void init_table();
public:
    explicit MainWindow(QTableView *_table, QWidget *parent = 0);
    ~MainWindow();

public slots:
    void show_current(QModelIndex current, QModelIndex previous);

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H

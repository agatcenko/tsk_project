#ifndef STARTWINDOW_H
#define STARTWINDOW_H

#include "startpage.h"
#include "initpage.h"
#include "connectpage.h"
#include "WizardArea.h"

#include <QWizard>

class StartWindow : public QWizard
{
    Q_OBJECT

public:
    StartWindow(QWidget *parent = 0);
};

#endif // STARTWINDOW_H

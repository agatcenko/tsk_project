#include "sqltablemodel.h"

#include "../src/MakeQuery.h"

#include <pqxx/pqxx>
#include <vector>
#include <string>

using namespace std;
using namespace pqxx;

SqlTableModel::SqlTableModel(shared_ptr<SqlAdapter> _sql, QStringList list, QObject *parent) :
    QAbstractTableModel(parent)
{
    sql = _sql;
    columns = list;
}

int SqlTableModel::rowCount(const QModelIndex &parent) const
{
    return numRows;
}

int SqlTableModel::columnCount(const QModelIndex &parent) const
{
    return numCols;
}

QVariant SqlTableModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (index.row() >= numRows || index.column() >= numCols)
        return QVariant();

    if (role == Qt::DisplayRole)
        return getValue(index.row(), index.column());
    else
        return QVariant();
}

QVariant SqlTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    if (orientation == Qt::Horizontal) {
        if (section < columns.size())
            return columns[section];
        else
            return QVariant();
    }
    else if (orientation == Qt::Vertical) {
        return QVariant(QString::number(section));
    }
}

TimeLineTableModel::TimeLineTableModel(shared_ptr<SqlAdapter> _sql, QStringList list, int max_elements, QObject *parent) :
    SqlTableModel(_sql, list, parent)
{
    max_elements_in_view = max_elements;

    result r = sql->select_from_table("SELECT count(*) FROM time_line;");

    numRows = r[0][0].as<int>();
    numCols = 6; // change
}

bool TimeLineTableModel::removeRows(int row, int count, const QModelIndex &parent)
{
    return false;
}

QVariant TimeLineTableModel::getValue(int row, int col) const
{
    int number = row / max_elements_in_view + 1;
    row -= (number - 1) * max_elements_in_view;
    string query = MakeQuery() << "SELECT * FROM time_line_view" << number << " OFFSET " << row << " LIMIT 1;";
    result r = sql->select_from_table(query);

    vector<QVariant> output (6);
    if (r.size() == 1) {
        output[0] = QString::fromStdString(r[0][1].as<string>());
        output[1] = QString::fromStdString(r[0][2].as<string>());
        output[2] = QString::fromStdString(r[0][3].as<string>());
        output[3] = QString::fromStdString(r[0][4].as<string>());
        output[4] = QString::fromStdString(r[0][5].as<string>());
        output[5] = QString::fromStdString(r[0][6].as<string>());
    }

    return output[col];
}

HistoryModel::HistoryModel(std::shared_ptr<SqlAdapter> _sql, QStringList list, QObject *parent) :
    SqlTableModel(_sql, list, parent)
{
    result r = sql->select_from_table("SELECT count(*) FROM history_table;");

    numRows = r[0][0].as<int>();
    numCols = 2; // change
}

bool HistoryModel::removeRows(int row, int count, const QModelIndex &parent)
{
    // work on it
    string query = MakeQuery() << "DELETE FROM history_table WHERE ctid IN (SELECT ctid FROM history_table OFFSET "
                               << row << " LIMIT " << count << ");";
    sql->execute_query(query);

    return true;
}

QVariant HistoryModel::getValue(int row, int col) const
{
    string query = MakeQuery() << "SELECT * FROM history_table OFFSET " << row << " LIMIT 1;";
    result r = sql->select_from_table(query);

    vector<QVariant> output (2);
    if (r.size() == 1) {
        output[0] = QString::fromStdString(r[0][1].as<string>());
        output[1] = QString::fromStdString(r[0][2].as<string>());
    }

    return output[col];
}

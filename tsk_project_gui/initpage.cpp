#include "initpage.h"

#include <string>
#include <sstream>

#include <QTableView>

using namespace std;

InitPage::InitPage(QWidget *parent) :
    QWizardPage(parent)
{
    setTitle(tr("Init Page"));

    image_button = new QPushButton(tr("Choose disk image"));
    init_time_line_button = new QPushButton(tr("Init!"));
    show_time_line_button = new QPushButton(tr("Show It!!!"));
    img_name_line = new QLineEdit();
    db_name_line = new QLineEdit();
    img_name_msg = new QLabel(tr("You have chosen: "));
    db_name_msg = new QLabel(tr("Name of data base will be: (You can change it)"));

    show_time_line_button->setDisabled(true);

    layout = new QVBoxLayout;
    layout->addWidget(image_button);
    layout->addWidget(img_name_msg);
    layout->addWidget(img_name_line);
    layout->addWidget(db_name_msg);
    layout->addWidget(db_name_line);
    layout->addWidget(init_time_line_button);
    layout->addWidget(show_time_line_button);
    setLayout(layout);

    connect(image_button, SIGNAL(clicked()), this, SLOT(choose_image()));
    connect(init_time_line_button, SIGNAL(clicked()), this, SLOT(init_time_line()));
    connect(show_time_line_button, SIGNAL(clicked()), this, SLOT(show_time_line()));
    connect(db_name_line, SIGNAL(textChanged(QString)), this, SLOT(change_db_name(QString)));
}

int InitPage::nextId() const
{
    return -1;
}

void InitPage::choose_image()
{
    img_name = QFileDialog::getOpenFileName(this, "Open file", "", "Files(*.*)");
    img_name_line->setText(img_name);

    generate_db_name();
}

void InitPage::generate_db_name()
{
    stringstream path(img_name.toStdString());
    string token;
    while (getline(path, token, '/'));

    stringstream with_point(token);
    string filename;
    getline(with_point, filename, '.');

    db_name = QString::fromStdString(filename);

    check_db_name_validity();
}

void InitPage::change_db_name(QString s)
{
    db_name = s;
    check_db_name_validity();
}

void InitPage::check_db_name_validity()
{
    string db_name_str;
    string name = db_name.toStdString();
    bool start_ok = false;
    for (size_t i = 0; i < name.size(); ++i) {
        if (!start_ok) {
            if (name[i] == '_' ||
                    (name[i] >= 'a' && name[i] <= 'z') ||
                    (name[i] >= 'A' && name[i] <= 'Z'))
            {
                start_ok = true;
                db_name_str.push_back(name[i]);
            }
        }
        else {
            if (name[i] == '_' || name[i] == '$' ||
                    (name[i] >= 'a' && name[i] <= 'z') ||
                    (name[i] >= 'A' && name[i] <= 'Z') ||
                    (name[i] >= '0' && name[i] <= '9'))
            {
                db_name_str.push_back(name[i]);
            }
        }
    }
    if (db_name_str.size() > 0) {
        db_name = QString::fromStdString(db_name_str);
        db_name_msg->setText("Good! This name is valid! But you still can change it, if you want!");
    }
    else {
        db_name = "";
        db_name_msg->setText(tr("Alert! This name is not valid for BD. Please, choose your own"));
    }

    db_name_line->setText(db_name);
}

void InitPage::init_time_line()
{
    sql = make_shared<SqlAdapter>(db_name.toStdString());
    sql->init();

    reader = make_shared<ImgReader>(img_name.toStdString(), sql);
    sql->create_mater_view_for_time_line("time_line_view", wizard_area::max_elements_in_view);

    history_sql = make_shared<SqlAdapter>("history_bd");
    history_sql->update_history(db_name.toStdString(), img_name.toStdString());

    show_time_line_button->setDisabled(false);
}

void InitPage::show_time_line()
{
    QStringList col_names;
    col_names << "Time" << "Name" << "Event" << "Size" << "Path" << "Type";
    m = new TimeLineTableModel(sql, col_names, wizard_area::max_elements_in_view);
    view = new QTableView;
    view->setModel(m);

    w = new MainWindow(view);
    w->show();
}

InitPage::~InitPage()
{
    delete image_button;
    delete init_time_line_button;
    delete show_time_line_button;
    delete img_name_line;
    delete db_name_line;
    delete img_name_msg;
    delete db_name_msg;
    delete layout;

    delete w;
    delete m;
    delete view;
}

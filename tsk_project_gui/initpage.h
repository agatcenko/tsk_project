#ifndef INITPAGE_H
#define INITPAGE_H

#include "WizardArea.h"
#include "mainwindow.h"
#include "sqltablemodel.h"
#include "../src/ImgReader.h"

#include <memory>

class InitPage : public QWizardPage
{
    Q_OBJECT

public:
    InitPage(QWidget* parent = 0);
    ~InitPage();

    int nextId() const;

private:
    QPushButton* image_button;
    QPushButton* init_time_line_button;
    QPushButton* show_time_line_button;
    QLineEdit* img_name_line;
    QLineEdit* db_name_line;
    QLabel* img_name_msg;
    QLabel* db_name_msg;
    QVBoxLayout* layout;
    QString img_name;
    QString db_name;

    std::shared_ptr<SqlAdapter> sql;
    std::shared_ptr<SqlAdapter> history_sql;
    std::shared_ptr<ImgReader> reader;

    TimeLineTableModel* m;
    QTableView* view;
    MainWindow* w;

    void generate_db_name();
    void check_db_name_validity();

protected slots:
    void choose_image();
    void show_time_line();
    void init_time_line();
    void change_db_name(QString s);
};

#endif // INITPAGE_H

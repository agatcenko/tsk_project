#ifndef SQLTABLEMODEL_H
#define SQLTABLEMODEL_H

#include "../src/SqlUtils.h"

#include <QWidget>
#include <QAbstractTableModel>

#include <memory>

class SqlTableModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    SqlTableModel(std::shared_ptr<SqlAdapter> _sql, QStringList list, QObject *parent = 0);

    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    virtual bool removeRows(int row, int count, const QModelIndex &parent) = 0;
protected:
    int numRows;
    int numCols;
    QStringList columns;
    std::shared_ptr<SqlAdapter> sql;

    virtual QVariant getValue(int row, int col) const = 0;
};

class TimeLineTableModel : public SqlTableModel
{
    Q_OBJECT

public:
    TimeLineTableModel(std::shared_ptr<SqlAdapter> _sql, QStringList list, int max_elements, QObject *parent = 0);
    bool removeRows(int row, int count, const QModelIndex &parent);
private:
    int max_elements_in_view;

    QVariant getValue(int row, int col) const;
};


class HistoryModel :public SqlTableModel
{
    Q_OBJECT

public:
    HistoryModel(std::shared_ptr<SqlAdapter> _sql, QStringList list, QObject *parent = 0);
    bool removeRows(int row, int count, const QModelIndex &parent);
private:
    QVariant getValue(int row, int col) const;
};

#endif // SQLTABLEMODEL_H

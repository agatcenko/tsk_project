#-------------------------------------------------
#
# Project created by QtCreator 2014-10-23T20:34:52
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = tsk_project_gui
TEMPLATE = app

QMAKE_CXXFLAGS += -std=c++11

LIBS += -ltsk
LIBS += -lpqxx
LIBS += -lpq

SOURCES += main.cpp\
        mainwindow.cpp \
        ../src/FileDescriptor.cpp \
        ../src/ImgReader.cpp \
        ../src/SqlUtils.cpp \
        ../src/Table.cpp \
        startwindow.cpp \
        startpage.cpp \
        initpage.cpp \
        connectpage.cpp \
        sqltablemodel.cpp

HEADERS  += mainwindow.h \
        ../src/FileDescriptor.h \
        ../src/ImgReader.h \
        ../src/MakeQuery.h \
        ../src/SqlUtils.h \
        ../src/Table.h \
        startwindow.h \
        startpage.h \
        WizardArea.h \
        initpage.h \
        connectpage.h \
        sqltablemodel.h

FORMS    += mainwindow.ui

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QTableView* _table, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    table = _table;
    setWindowState(Qt::WindowMaximized);

    init_table();

    selection = new QItemSelectionModel(table->model());
    table->setSelectionModel(selection);

    ui->verticalLayout->addWidget(table);
    connect(selection, SIGNAL(currentRowChanged(QModelIndex,QModelIndex)), this, SLOT(show_current(QModelIndex, QModelIndex)));
}

void MainWindow::init_table()
{
    table->horizontalHeader()->setStretchLastSection(true);
    table->setColumnWidth(0, 150); // Time
    table->setColumnWidth(1, 300); // Name
    table->setColumnWidth(2, 120); // Event
    table->setColumnWidth(3, 100); // Size
    table->setColumnWidth(4, 400); // Path
}

void MainWindow::show_current(QModelIndex current, QModelIndex previous)
{
    ui->current_name->setText(table->model()->data(table->model()->index(current.row(), 1)).toString());
    ui->current_path->setText(table->model()->data(table->model()->index(current.row(), 4)).toString());
}

MainWindow::~MainWindow()
{
    delete table;
    delete selection;

    delete ui;
}

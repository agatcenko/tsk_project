#include "startpage.h"

StartPage::StartPage(QWidget *parent) :
    QWizardPage(parent)
{
    setTitle(tr("Start page"));

    init_button = new QRadioButton(tr("First Time? Choose this variant ;)"));
    connect_button = new QRadioButton(tr("Do you wanna try again? Come here!"));

    init_button->setChecked(true);

    layout = new QVBoxLayout;
    layout->addWidget(init_button);
    layout->addWidget(connect_button);
    setLayout(layout);
}

int StartPage::nextId() const
{
    if (init_button->isChecked())
        return wizard_area::Page_Init;
    if (connect_button->isChecked())
        return wizard_area::Page_Connect;

    return -1;
}

StartPage::~StartPage()
{
    delete init_button;
    delete connect_button;
    delete layout;
}

#ifndef MAKE_QUERY_H
#define MAKE_QUERY_H

#include <sstream>
#include <string>

class MakeQuery {
    std::stringstream stream;
public:
    template <class T>
    MakeQuery& operator<< (const T& arg);
    inline operator std::string() const;
};

template <class T>
MakeQuery& MakeQuery::operator<<(const T& arg)
{
    stream << arg;
    return *this;
}

inline MakeQuery::operator std::string() const
{
    return stream.str();
}

#endif
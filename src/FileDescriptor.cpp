#include "FileDescriptor.h"
#include "MakeQuery.h"
#include <iostream>
#include <sstream>
#include <string>
#include <cstring>
#include <ctime>
#include <time.h>

using namespace std;

FileDescriptor::FileDescriptor()
{

}

void FileDescriptor::get_attributes(TskFsFile* fs_file, const char* path)
{
    name = fs_file->getName()->getName();
    full_path = path;

    check_value_for_quotes(name);
    check_value_for_quotes(full_path);

    accessed_time = fs_file->getMeta()->getATime();
    backup_time = fs_file->getMeta()->getBackUpTime();
    created_time = fs_file->getMeta()->getCrTime();
    changed_time = fs_file->getMeta()->getCTime();
    modification_time = fs_file->getMeta()->getMTime();

    flags = fs_file->getMeta()->getFlags();
    type = meta_type_name[fs_file->getMeta()->getType()];
    group_id = fs_file->getMeta()->getGid();
    owner_id = fs_file->getMeta()->getUid();
    size = fs_file->getMeta()->getSize();

    make_times_readable();
}

void FileDescriptor::check_value_for_quotes(string &value)
{
    const char* quote = "'";
    size_t value_size = value.size();
    for (size_t i = 0; i < value_size; ++i) {
        string letter (1, value[i]);
        if (strcmp(letter.c_str(), quote) == 0) {
            value.insert(i++, "'");
            value_size++;
        }
    }
}

void FileDescriptor::make_times_readable()
{
    strftime(accessed_time_str, 100, "%Y/%m/%d %H:%M:%S", gmtime(&accessed_time));
    strftime(backup_time_str, 100, "%Y/%m/%d %H:%M:%S", gmtime(&backup_time));
    strftime(created_time_str, 100, "%Y/%m/%d %H:%M:%S", gmtime(&created_time));
    strftime(changed_time_str, 100, "%Y/%m/%d %H:%M:%S", gmtime(&changed_time));
    strftime(modification_time_str, 100, "%Y/%m/%d %H:%M:%S", gmtime(&modification_time));
}

std::string FileDescriptor::get_string_of_attributes() const
{
    stringstream values;
    values << "\'" << name << "\', " << size << ", \'" << accessed_time_str << "\', \'" <<
    backup_time_str << "\', \'" << created_time_str << "\', \'" << changed_time_str << "\', \'" <<
    modification_time_str << "\', " << flags << ", \'" << type << "\', " << group_id << ", " <<
    owner_id << ",  \'" << full_path << "\'";
    //cout << "MAKE QUERY!! >>> " << values.str() << endl;

    return values.str();
}

std::vector<std::string> FileDescriptor::get_values_of_times() const
{
    vector<string> times;

    times.push_back(MakeQuery() << "DEFAULT, \'" << accessed_time_str << "\', \'" << name << "\', 'ACCESS', " <<
                    size << ", \'" << full_path << "\', \'" << type << "\'");
    times.push_back(MakeQuery() << "DEFAULT, \'" << backup_time_str << "\', \'" << name << "\', 'BACKUP', " <<
                    size << ", \'" << full_path << "\', \'" << type << "\'");
    times.push_back(MakeQuery() << "DEFAULT, \'" << created_time_str << "\', \'" << name << "\', 'CREATE', " <<
                    size << ", \'" << full_path << "\', \'" << type << "\'");
    times.push_back(MakeQuery() << "DEFAULT, \'" << changed_time_str << "\', \'" << name << "\', 'CHANGE', " <<
                    size << ", \'" << full_path << "\', \'" << type << "\'");
    times.push_back(MakeQuery() << "DEFAULT, \'" << modification_time_str << "\', \'" << name << "\', 'MODIFICATION', " <<
                    size << ", \'" << full_path << "\', \'" << type << "\'");

    return times;
}

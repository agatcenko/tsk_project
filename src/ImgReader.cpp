#include "ImgReader.h"
#include "MakeQuery.h"

#include <iostream>

using namespace std;

ImgReader::ImgReader(std::string name, std::shared_ptr<SqlAdapter> _sql, TSK_IMG_TYPE_ENUM type, unsigned int size)
{
    ImgReader((const TSK_TCHAR *)name.c_str(), _sql, type, size);
}

ImgReader::ImgReader(const TSK_TCHAR* name, std::shared_ptr<SqlAdapter> _sql, TSK_IMG_TYPE_ENUM type, unsigned int size)
{
    img_name = name;
    img_type = type;
    img_size = size;
    sql = _sql;

    img_info = make_shared<TskImgInfo>();

    if (img_info->open(img_name, img_type, img_size)) {
        cout << "Error while openning image!" << endl;
    }
    
    read_img();
}

ImgReader::~ImgReader()
{

}

int ImgReader::read_img()
{
    if (analyze_volume_system(img_info.get(), (TSK_DADDR_T) 0)) {
        cout << "Error while analyze_volume_system" << endl;
        return 1;
    }
    
    cout << "End of read_img!" << endl;
    return 0;
}

int ImgReader::analyze_file_system(TskImgInfo* img_info, TSK_DADDR_T start_pos)
{
    shared_ptr<TskFsInfo> fs_info(new TskFsInfo());

    if (fs_info->open(img_info, start_pos, TSK_FS_TYPE_DETECT)) {
        cout << "Error while openning file system" << endl;
        return 1;
    }

    shared_ptr<TSK_STACK> stack(tsk_stack_create());
    
    if (analyze_directory(fs_info.get(), stack.get(), fs_info->getRootINum(), "")) {
        cout << "Error while analyzing directory" << endl;
        return 1;
    }
    cout << "before free stack" << endl;
    //tsk_stack_free(stack.get());
    cout << "after free stack" << endl;

    return 0;
}

int ImgReader::analyze_volume_system(TskImgInfo* img_info, TSK_DADDR_T start_pos)
{
    shared_ptr<TskVsInfo> vs_info(new TskVsInfo());

    if (vs_info->open(img_info, start_pos, TSK_VS_TYPE_DETECT)) {
        cout << "Error while openning volume system" << endl;
        // try open something another
        if (analyze_file_system(img_info, start_pos)) {
            return 1;
        }
        cout << "analyze_file_system good end!" << endl;
    }
    else {
        cout << "volume system is opened" << endl;
        for (TSK_PNUM_T i = 0; i < vs_info->getPartCount(); ++i) {
            const TskVsPartInfo *vs_part;
            
            if ((vs_part = vs_info->getPart(i)) == NULL) {
                cout << "Error while openning part of volume system" << endl;
                continue;
            }

            // ignore the metadata partitions
            if (const_cast<TskVsPartInfo *>(vs_part)->getFlags() & TSK_VS_PART_FLAG_META) {
                continue;
            }

            // could do something with unallocated volumes
            else if (const_cast<TskVsPartInfo *>(vs_part)->getFlags() & TSK_VS_PART_FLAG_UNALLOC) {

            }
            else {
                if (analyze_file_system(img_info, 
                    const_cast<TskVsPartInfo *>(vs_part)->getStart() * vs_info->getBlockSize())) {
                    // We could do more fancy error checking here to see the cause 
                    // of the error or consider the allocation status of the volume...
                    cout << "analyze_file_system was failed" << endl;
                }
            }
        }
        vs_info->close();
    }

    return 0;
}

int ImgReader::analyze_directory(TskFsInfo* fs_info, TSK_STACK* stack, TSK_INUM_T dir_inum, const char *path)
{
    shared_ptr<TskFsDir> fs_dir(new TskFsDir());
    if (fs_dir->open(fs_info, dir_inum)) {
        cout << "Error while openning directory" << endl;
        return 1;
    } 
    char* buffer = new char[2048];
    char* next_path = new char[4096];
    for (size_t i = 0; i < fs_dir->getSize(); ++i) {
        shared_ptr<TskFsFile> fs_file(fs_dir->getFile(i));
        if (fs_file.get() == NULL) {
            cout << "Error while openning file" << endl;
            delete[] buffer;
            delete[] next_path;
            return 1;
        }
        if (fs_file->getMeta()) {
            //if (fs_file->getMeta()->getType() == TSK_FS_META_TYPE_REG) {
                /*TSK_OFF_T file_size = fs_file->getMeta()->getSize();
                size_t len = 0;
                int flags = 0;
                for (TSK_OFF_T offset = 0; offset < file_size; offset += len) {
                    if (file_size - offset < 2048) {
                        len = (size_t)(file_size - offset); 
                    }
                    else {
                        len = 2048;
                    }

                    ssize_t cnt = fs_file->read(offset, buffer, len, (TSK_FS_FILE_READ_FLAG_ENUM) flags);
                    if (cnt == -1) {
                        cout << "Error while reading file" << endl;
                        break;
                    }
                    else if (cnt != (ssize_t)len) {
                        cout << "Warning with count of reading bytes!" << endl;
                    }
                }*/
                if (!(fs_file->getMeta()->getType() == TSK_FS_META_TYPE_DIR &&
                      TSK_FS_ISDOT(fs_file->getName()->getName()) == 1)) {
                    FileDescriptor file;
                    file.get_attributes(fs_file.get(), path);
                    try {
                        sql->insert_into_table(*sql->files_table.get(), MakeQuery() << "DEFAULT, " << file.get_string_of_attributes());
                        sql->multi_insert_into_table(*sql->time_line_table.get(), file.get_values_of_times());
                    }
                    catch (const exception& e) {
                        cout << "Exception! " << e.what() << endl;
                        return 1;
                    }
                }
            //}
                if (fs_file->getMeta()->getType() == TSK_FS_META_TYPE_DIR) {
                    if (TSK_FS_ISDOT(fs_file->getName()->getName()) == 0) {
                        TSK_INUM_T addr = fs_file->getMeta()->getAddr();
                        if (tsk_stack_find(stack, addr) == 0) {
                            tsk_stack_push(stack, addr);
                            // work with path
                            snprintf(next_path, 4096, "%s/%s", path, fs_file->getName()->getName());
                            if (analyze_directory(fs_info, stack, addr, next_path)) {
                                fs_file->close();
                                fs_dir->close();
                                delete[] buffer;
                                delete[] next_path;
                                return 1;
                            }
                            tsk_stack_pop(stack);
                        }
                    }
                }
        }
        fs_file->close();
    }
    fs_dir->close();
    delete[] buffer;
    delete[] next_path;
    return 0;
}

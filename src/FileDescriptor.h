#ifndef FILE_DESCRIPTOR_H
#define FILE_DESCRIPTOR_H

#include <tsk/libtsk.h>
#include <ctime>
#include <vector>
#include <string>

class FileDescriptor {
    std::string name;

    time_t accessed_time;
    time_t backup_time;
    time_t created_time;
    time_t changed_time;
    //time_t deletion_time; // ???
    time_t modification_time;

    char accessed_time_str[100];
    char backup_time_str[100];
    char created_time_str[100];
    char changed_time_str[100];
    //char deletion_time_str[100]; // ???
    char modification_time_str[100];

    TSK_FS_META_FLAG_ENUM flags;
    TSK_GID_T group_id;
    TSK_UID_T owner_id;
    TSK_OFF_T size;

    std::string type;
    std::string full_path;

    void make_times_readable();
    void check_value_for_quotes(std::string &value);

public:
    FileDescriptor();
    void get_attributes(TskFsFile* fs_file, const char *path);
    std::string get_string_of_attributes() const;
    std::vector<std::string> get_values_of_times() const;
};

const std::string meta_type_name[] = {
    "Undefined file",
    "Regular file",
    "Directory file",
    "Named pipe (fifo)",
    "Character device",
    "Block device",
    "Symbolic link",
    "Solaris only",
    "UNIX domain socket",
    "Whiteout",
    "Virtual file by TSK"
};

#endif

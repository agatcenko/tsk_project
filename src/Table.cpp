#include <iostream>
#include <sstream>

#include "Table.h"

using namespace std;

Table::Table(string table_name, string col_names, string col_types)
{
    name = table_name;
    col_names_str = col_names;

    string name;
    stringstream names(col_names);
    while(getline(names, name, ',')) {
        col_name.push_back(name);
    }

    string type;
    stringstream types(col_types);
    while(getline(types, type, ',')) {
        col_type.push_back(type);
    }

    if (col_name.size() != col_type.size()) {
        cout << "Number of types and number of names don`t match in table " << name << endl; // Make exception instead
    }
}

string Table::get_table_name() const
{
    return name;
}

string Table::get_col_name(unsigned int number) const
{
    return col_name.at(number);
}

string Table::get_col_type(unsigned int number) const
{
    return col_type.at(number);
}

string Table::make_create_query() const
{
    stringstream create_query;
    create_query << "CREATE TABLE " << name << "(";
    for (size_t i = 0; i < col_type.size(); ++i) {
        create_query << " " << col_name[i] << " " << col_type[i];
        if (i != col_type.size() - 1)
            create_query << ",";
    }
    create_query << ");";
    
    return create_query.str();
}

std::string Table::make_drop_query() const
{
    stringstream drop_query;
    drop_query << "DROP TABLE " << name << ";";

    return drop_query.str();
}

string Table::make_insert_query(string values_str) const
{
    stringstream insert_query;
    insert_query << "INSERT INTO " << name << "(" << col_names_str << ") ";
    insert_query << "VALUES (" << values_str << ");";

    return insert_query.str();
}

string Table::make_multi_insert_query(vector<string> values) const
{
    stringstream multi_insert_query;
    multi_insert_query << "INSERT INTO " << name << "(" << col_names_str << ") VALUES ";
    for (size_t i = 0; i < values.size(); ++i) {
        multi_insert_query << "(" << values[i] << ")";
        if (i != values.size() - 1) {
            multi_insert_query << ", ";
        }
    }
    multi_insert_query << ";";

    return multi_insert_query.str();
}

string Table::make_select_all_query() const
{
    stringstream select_all_query;
    select_all_query << "SELECT * FROM " << name << ";";

    return select_all_query.str();
}

FilesTable::FilesTable(std::string name, std::string col_names, std::string col_types) :
    Table(name, col_names, col_types)
{

}

TimeLineTable::TimeLineTable(std::string name, std::string col_names, std::string col_types) :
    Table(name, col_names, col_types)
{
    
}

string TimeLineTable::make_select_query(string order_col, uint limit, uint offset)
{
    stringstream select_query;
    select_query << "SELECT * FROM " << name << " ORDER BY " << order_col << " ASC ";
    if (limit != 0) {
        select_query << "LIMIT " << limit << " OFFSET " << offset;
    }
    select_query << ";";

    return select_query.str();
}
#include "SqlUtils.h"
#include "MakeQuery.h"

#include <pqxx/pqxx>
#include <iostream>
#include <sstream>
#include <string>
#include <math.h>

using namespace std;
using namespace pqxx;

SqlAdapter::SqlAdapter()
{
    db_name = "default";
}

SqlAdapter::SqlAdapter(const char* name)
{
    db_name = name;
}

SqlAdapter::SqlAdapter(string name)
{
    db_name = name;
}

int SqlAdapter::check_database()
{
    string connection_params = "dbname = template1 user = postgres";
    connection db(connection_params);
    nontransaction tran(db);

    bool need_new_db = false;

    result res = tran.exec(MakeQuery() << "select count(*) from pg_catalog.pg_database where datname = "
                                       << tran.quote<string>(db_name));
    if (res.size() == 0 || res[0][0].as<int>() == 0) {
        cout << db_name << " not exists" << endl;
        need_new_db = true;
    }
    else {
        cout << db_name << " exists" << endl;
        //return 1;
    }

    if (need_new_db) {
        tran.exec(MakeQuery() << "CREATE DATABASE " << db_name << " WITH OWNER = postgres");
    }

    db_con = make_shared<connection>(MakeQuery() << "dbname = " << db_name << " user = postgres");

    if (db_con->is_open()) {
        cout << db_name << " is opened" << endl;
    }

    return 0;
}

int SqlAdapter::init()
{
    try {
        check_database();

        // Delete existing tables
        work w(*db_con.get());
        result res = w.exec(MakeQuery() << "select exists (select relname from pg_class where relname = " <<
                            w.quote<string>("files") << " and relkind = " << w.quote<string>("r") << " );");
        if (res[0][0].as<string>() == "t")
            w.exec(MakeQuery() << "DROP TABLE files;");

        int iter = 1;
        while(1) {
           string view_name = "time_line_view";
           stringstream ss;
           ss << view_name << iter;
           res = w.exec(MakeQuery() << "select exists (select relname from pg_class where relname = " <<
                               w.quote<string>(ss.str()) << " and relkind = " << w.quote<string>("mv") << " );");
           if (res[0][0].as<string>() == "t") {
               w.exec(MakeQuery() << "DROP MATERIALIZED VIEW time_line_view" << iter);
               iter++;
           }
           else {
               break;
           }
        }

        res = w.exec(MakeQuery() << "select exists (select relname from pg_class where relname = " <<
                            w.quote<string>("time_line") << " and relkind = " << w.quote<string>("r") << " );");
        if (res[0][0].as<string>() == "t")
            w.exec(MakeQuery() << "DROP TABLE time_line");

        w.commit();

        string files_table_names = "id,name,size,acc_t,backup_t,cr_t,ch_t,modif_t,flags,type,group_id,owner_id,path";
        string files_table_types = "BIGSERIAL PRIMARY KEY,TEXT,BIGINT,TIMESTAMP,TIMESTAMP,TIMESTAMP,TIMESTAMP,TIMESTAMP,INTEGER,TEXT,INTEGER,INTEGER,TEXT";
        files_table = make_shared<FilesTable>("files", files_table_names, files_table_types);

        create_table(*files_table.get());

        cout << "Files table was created!" << endl;

        string time_line_table_names = "id, time, file_name, event, size, path, type";
        string time_line_table_types = "BIGSERIAL PRIMARY KEY, TIMESTAMP, TEXT, TEXT, INTEGER, TEXT, TEXT";
        time_line_table = make_shared<TimeLineTable>("time_line", time_line_table_names, time_line_table_types);

        create_table(*time_line_table.get());

        cout << "Time line table was created!" << endl;
    }
    catch (const exception &e) {
        cout << "Exception: " << e.what() << endl;
        return 1;
    }

    return 0;   
}

int SqlAdapter::connect()
{
    try {
        db_con = make_shared<connection>(MakeQuery() << "dbname = " << db_name << " user = postgres");

        if (db_con->is_open()) {
            cout << db_name << " is opened" << endl;
        }
    }
    catch (const exception &e) {
        cout << "Exception: " << e.what() << endl;
        return 1;
    }

    return 0;
}

int SqlAdapter::update_history(string _bd_name, string _img_name)
{
    try {
        check_database();

        string table_history_names = "id, db_name, img_name";
        string table_history_types = "BIGSERIAL PRIMARY KEY, TEXT, TEXT";
        history_table = make_shared<Table>("history_table", table_history_names, table_history_types);

        work w(*db_con.get());
        result res = w.exec(MakeQuery() << "select exists (select relname from pg_class where relname = " <<
                            w.quote<string>("history_table") << " and relkind = " << w.quote<string>("r") << " );");
        w.commit();

        if (res[0][0].as<string>() == "f")
            create_table(*history_table.get());

        nontransaction tr(*db_con.get());
        res = tr.exec(MakeQuery() << "select * from history_table where db_name = " <<
                     w.quote<string>(_bd_name) << " and img_name = " << w.quote<string>(_img_name) << ";");
        tr.commit();
        if (res.size() == 0 || res[0][0].as<int>() == 0)
            insert_into_table(*history_table.get(), MakeQuery() << "DEFAULT, \'" << _bd_name << "\', \'" << _img_name << "\'");
    }
    catch (const exception &e) {
        cout << "Exception: " << e.what() << endl;
        return 1;
    }

    return 0;
}

int SqlAdapter::create_table(Table t)
{
    work w(*db_con.get());

    w.exec(t.make_create_query());
    w.commit();

    return 0;
}

int SqlAdapter::drop_table(Table t) 
{
    work w(*db_con.get());
    w.exec(t.make_drop_query());
    w.commit();

    return 0;
}

int SqlAdapter::insert_into_table(Table t, string values)
{
    work w(*db_con.get());
    w.exec(t.make_insert_query(values));
    w.commit();

    return 0;
}

int SqlAdapter::multi_insert_into_table(Table t, vector<string> values)
{
    work w(*db_con.get());
    w.exec(t.make_multi_insert_query(values));
    w.commit();

    return 0;
}

result SqlAdapter::select_all_from_files_table(FilesTable t)
{
    nontransaction tr(*db_con.get());

    return tr.exec(t.make_select_all_query()); 
}

int SqlAdapter::create_mater_view(Table t, std::string view_name, std::string params, int max_elements)
{
    nontransaction w(*db_con.get());
    string query = MakeQuery() << "CREATE MATERIALIZED VIEW " << view_name 
        << " AS SELECT * FROM " << t.get_table_name() << " " << params << " LIMIT " << max_elements << ";";
    w.exec(query);

    return 0;
}

int SqlAdapter::create_mater_view_for_time_line(std::string view_name, int max_elemetns)
{
    result r = select_from_table("SELECT count(*) FROM time_line;");
    int total_rows = r[0][0].as<int>();
    int views_number = ceil(total_rows / (double)max_elemetns);
    for (int i = 0; i < views_number; ++i) {
        stringstream ss;
        ss << view_name << i + 1;
        string params = MakeQuery() << "ORDER BY time ASC OFFSET " << i * max_elemetns;
        create_mater_view(*time_line_table.get(), ss.str(), params , max_elemetns);
    }

    return 0;
}

pqxx::result SqlAdapter::select_from_table(std::string query)
{
    read_transaction tr(*db_con.get());

    return tr.exec(query);
}

int SqlAdapter::execute_query(std::string query)
{
    work w(*db_con.get());
    w.exec(query);
    w.commit();

    return 0;
}

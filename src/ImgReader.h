#ifndef IMG_READER_H
#define IMG_READER_H

#include <tsk/libtsk.h>
#include <memory>
#include <string>

#include "FileDescriptor.h"
#include "SqlUtils.h"

class ImgReader {
private:
    const TSK_TCHAR* img_name;
    TSK_IMG_TYPE_ENUM img_type;
    unsigned int img_size;
    std::shared_ptr<TskImgInfo> img_info;
    std::shared_ptr<SqlAdapter> sql;

    int analyze_file_system(TskImgInfo* img_info, TSK_DADDR_T start_pos);
    int analyze_volume_system(TskImgInfo* img_info, TSK_DADDR_T start_pos);
    int analyze_directory(TskFsInfo* fs_info, TSK_STACK* stack, TSK_INUM_T dir_inum, const char *path);

public:
    ImgReader(std::string name, std::shared_ptr<SqlAdapter> _sql, TSK_IMG_TYPE_ENUM type = TSK_IMG_TYPE_DETECT, unsigned int size = 0);
    ImgReader(const TSK_TCHAR* name, std::shared_ptr<SqlAdapter> _sql, TSK_IMG_TYPE_ENUM type = TSK_IMG_TYPE_DETECT, unsigned int size = 0);
    ~ImgReader();

    int read_img();
};

#endif
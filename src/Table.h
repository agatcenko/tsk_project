#ifndef TABLE_H
#define TABLE_H

#include <string>
#include <vector>

class Table {
protected:
    std::string name;
    std::string col_names_str;
    std::vector<std::string> col_name;
    std::vector<std::string> col_type;
public:
    std::string get_table_name() const;
    std::string get_col_name(unsigned int number) const;
    std::string get_col_type(unsigned int number) const;
    std::string make_create_query() const;
    std::string make_drop_query() const;
    std::string make_insert_query(std::string values_str) const;
    std::string make_multi_insert_query(std::vector<std::string> values) const;
    std::string make_select_all_query() const;
    
    Table(std::string name, std::string col_names, std::string col_types);
};

#endif

class FilesTable : public Table {
public:
    FilesTable(std::string name, std::string col_names, std::string col_types);
};

class TimeLineTable : public Table {
public:
    std::string make_select_query(std::string order_col, uint limit, uint offset);

    TimeLineTable(std::string name, std::string col_names, std::string col_types);
};
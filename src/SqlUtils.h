#ifndef SQL_UTILS_H
#define SQL_UTILS_H

#include <pqxx/pqxx>
#include <vector>
#include <memory>
#include <string>

#include "Table.h"

struct TimeLineRow {
    uint id;
    std::string time;
    std::string file_name;
    std::string event;
};

class SqlAdapter {
private:
    std::shared_ptr<pqxx::connection> db_con;
    std::string db_name;

    int check_database();
    int create_mater_view(Table t, std::string view_name, std::string params, int max_elements);
public:
    std::shared_ptr<Table> history_table;
    std::shared_ptr<FilesTable> files_table;
    std::shared_ptr<TimeLineTable> time_line_table;

    SqlAdapter(); // create BD
    SqlAdapter(const char* name);
    SqlAdapter(std::string name);

    int init();
    int connect();
    int create_table(Table t);
    int drop_table(Table t);
    int insert_into_table(Table t, std::string values);
    int multi_insert_into_table(Table t, std::vector<std::string> values);
    pqxx::result select_all_from_files_table(FilesTable t);
    pqxx::result select_from_table(std::string query);
    int execute_query(std::string query);

    int create_mater_view_for_time_line(std::string view_name, int max_elemetns);
    int update_history(std::string _bd_name, std::string _img_name);
};

#endif

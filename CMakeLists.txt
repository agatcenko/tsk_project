cmake_minimum_required(VERSION 2.8.3)

add_definitions(--std=c++11 -Wall)

project(tsk_project)
file(GLOB_RECURSE SOURCES "src/*.cpp")

add_executable(${PROJECT_NAME} ${SOURCES})

target_link_libraries(${PROJECT_NAME}
    tsk
    pqxx
    pq  
)
